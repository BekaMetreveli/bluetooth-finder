package com.example.bluetoothfinder

import android.Manifest
import android.app.AlertDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var bluetoothAdapter: BluetoothAdapter
    private lateinit var adapter: RecyclerViewAdapter
    private var devices = mutableListOf<Device>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION = 1

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {  // Only ask for these permissions on runtime when running Android 6.0 or higher
            when (ContextCompat.checkSelfPermission(
                baseContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            )) {
                PackageManager.PERMISSION_DENIED -> (AlertDialog.Builder(this)
                    .setTitle("Runtime Permissions up ahead")
                    .setMessage(
                        Html.fromHtml(
                            "<p>To find nearby bluetooth devices please click \"Allow\" on the runtime permissions popup.</p>" +
                                    "<p>For more info see <a href=\"http://developer.android.com/about/versions/marshmallow/android-6.0-changes.html#behavior-hardware-id\">here</a>.</p>"
                        )
                    )
                    .setNeutralButton("Okay", DialogInterface.OnClickListener { dialog, which ->
                        if (ContextCompat.checkSelfPermission(
                                baseContext,
                                Manifest.permission.ACCESS_FINE_LOCATION
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            ActivityCompat.requestPermissions(
                                this@MainActivity,
                                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                MY_PERMISSIONS_REQUEST_ACCESS_COARSE_LOCATION
                            )
                        }
                    })
                    .show()
                    .findViewById(android.R.id.message) as TextView).movementMethod =
                    LinkMovementMethod.getInstance() // Make the link clickable. Needs to be called after show(), in order to generate hyperlinks
                PackageManager.PERMISSION_GRANTED -> {
                }
            }
        }

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

        val intentFilter = IntentFilter()
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED)
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND)
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
        registerReceiver(broadcastReceiver, intentFilter)
    }

    fun searchClicked(view: View) {
        adapter = RecyclerViewAdapter(emptyList())
        devices.clear()
        adapter.setAdapterData(emptyList())
        statusTV.text = "Searching..."
        view.isEnabled = false
        bluetoothAdapter.startDiscovery()
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val action = p1!!.action

            Log.d("action", action!!)
            if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED == action) {
                statusTV.text = "Finished"
                button.isEnabled = true
                adapter = RecyclerViewAdapter(emptyList())
                adapter.setAdapterData(devices)
                recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)
                recyclerView.adapter = adapter
            } else if (BluetoothDevice.ACTION_FOUND == action) {
                val device = p1.getParcelableExtra<BluetoothDevice>(BluetoothDevice.EXTRA_DEVICE)
                val name = device?.name.toString()
                val address = device?.address.toString()
                val rssi = p1.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE).toString()
                devices.add(Device(name, address, rssi))
            }
        }
    }

    data class Device(val name: String, val address: String, val rssi: String)

}